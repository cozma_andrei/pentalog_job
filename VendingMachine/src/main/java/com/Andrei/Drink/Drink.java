package com.Andrei.Drink;

/**
 * Class that stores the drink attributes
 * @author acozma
 *
 */
public class Drink
{
  private String name;
  private int id;
  private double price;

  /**
   * Constructor
   * @param id - the id of the drink
   * @param name - the name of the drink
   * @param price - the price of the drink
   */
  public Drink(int id, String name, double price)
  {
    this.id = id;
    this.name = name;
    this.price = price;
  }

  public String getName()
  {
    return this.name;
  }

  public double getPrice() {
    return this.price;
  }

  public int getId() {
    return this.id;
  }
  

}