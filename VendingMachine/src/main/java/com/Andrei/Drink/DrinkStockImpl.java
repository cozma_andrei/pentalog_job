package com.Andrei.Drink;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * Class that manages the drink stock
 * @param drinkStock -  maps the drink type with its quantity and stores the entire drink stock
 * @author acozma
 *
 */
@SuppressWarnings("rawtypes")
public class DrinkStockImpl implements DrinkStock {
	 private  Map<Drink,Integer> drinkStock;
	 
	public Map<Drink, Integer> getDrinkStock() {
		return drinkStock;
	}

	public void setDrinkStock(Map<Drink, Integer> drinkStock) {
		this.drinkStock = drinkStock;
	}

	public DrinkStockImpl()
	{
		this.drinkStock = new HashMap<Drink, Integer>();
	}
	
	public DrinkStockImpl(Map<Drink,Integer> drinkStock)
	{
		this.drinkStock = drinkStock;
	}
	
	
	@Override
	public void addDrinksToStock(Map<Drink, Integer> drinksToAdd) 
	{
		for (Iterator it = drinksToAdd.entrySet().iterator(); it.hasNext(); )
	    {
	      Map.Entry p = (Map.Entry)it.next();

	      Integer oldQuantity = (Integer)this.drinkStock.get(p.getKey());
	      Integer quantity = (Integer)p.getValue();
	      this.drinkStock.put((Drink)p.getKey(), Integer.valueOf(oldQuantity.intValue() + quantity.intValue()));
	    }
		
	}
	@Override
	public void removeDrinksFromStock(Map<Drink, Integer> drinksToAdd) 
	{
		for (Iterator it = drinksToAdd.entrySet().iterator(); it.hasNext(); )
	    {
	      Map.Entry p = (Map.Entry)it.next();

	      Integer oldQuantity = (Integer)this.drinkStock.get(p.getKey());
	      Integer quantity = (Integer)p.getValue();
	      this.drinkStock.put((Drink)p.getKey(), Integer.valueOf(oldQuantity.intValue() - quantity.intValue()));
	    }
		
	}
	  /**
	   * @see DrinkStock#isAvailable
	   */
	  @Override
	  public boolean isAvailable(String aDrink)
	  {
		  for (Iterator it = this.getDrinkStock().entrySet().iterator(); it.hasNext(); )
		    {
		      Map.Entry p = (Map.Entry)it.next();
		      Drink d = (Drink)p.getKey();
		      Integer value = (Integer)p.getValue();
		      if(d.getName().equals(aDrink))
		      { 
		    	 if(value>0) return true;
		    	 else return false;
		      }
		      
		    }
		  return false;
	  }
	  /**
	   * @see DrinkStock#getDrink
	   */
	  @Override
	  public Drink getDrink(String drinkName)
	  {
		  Drink drink =null;
		  for (Iterator it = this.getDrinkStock().entrySet().iterator(); it.hasNext(); )
		    {
		      Map.Entry p = (Map.Entry)it.next();
		      Drink d = (Drink)p.getKey();
		      if(d.getName().equals(drinkName))
		      { 
		    	 drink = d;
		    	 break;
		      }
		      
		    }
		  return drink;
	  }
	  
	  /**
	   * @see DrinkStock#extractDrink
	   */
	  @Override
	  public Drink extractDrink(String drinkName)
	  {
		  Drink drink =null;
		  for (Iterator it = this.getDrinkStock().entrySet().iterator(); it.hasNext(); )
		    {
		      Map.Entry p = (Map.Entry)it.next();
		      Drink d = (Drink)p.getKey();
		      Integer quantity = (Integer)p.getValue();
		      if(d.getName().equals(drinkName))
		      { 
		    	 drink = d;
		    	 this.getDrinkStock().put(d, quantity-1);
		    	 break;
		      }
		      
		    }
		  return drink;
	  }
		 public void revert(String drinkName) {
			  Drink drink = getDrink(drinkName);
			  drinkStock.put(drink,drinkStock.get(drink) + 1);
			 }
}

