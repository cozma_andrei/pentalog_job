package com.Andrei.Drink;

import java.util.Map;

/**
 * Interface that manages the stock of drinks.
 * @author acozma
 *
 */
public interface DrinkStock {
	
	public Map<Drink, Integer> getDrinkStock();
	
	public void setDrinkStock(Map<Drink,Integer> drinkStock);
	
	/**
	 * Add a number of Drinks to the Drink stock.
	 * @param drinksToAdd - a map containing a number of drinks to add to the stock
	 */
	public void addDrinksToStock(Map<Drink, Integer> drinksToAdd);
	/**
	 * Remove a number of Drinks from the Drink stock.
	 * @param DrinksToAdd - a map containing a number of drinks to remove from the stock
	 */
	public void removeDrinksFromStock(Map<Drink, Integer> drinksToAdd);
	
	  /**
	   * Method that verifies the availability of a stock of a given drink.
	   * @param aDrink
	   * @return true if the drink is available
	   */
	  public boolean isAvailable(String aDrink);

	  /**
	   * Method that returns the drink with the drink name
	   * @param drinkName - the drink name
	   * @return the drink
	   */
	  public Drink getDrink(String drinkName);
	/**
	 * Method that extracts the drink from the drink stock and gives it to the user
	 * @param drinkName - the name of the drink
	 * @return - the drink
	 */
	public Drink extractDrink(String drinkName);

	/**
	 * Revert changes in drink stock by putting back the drink 
	 * @param drinkName - the drink name
	 */
	public void revert(String drinkName);
}
