package com.Andrei.app;

import com.Andrei.Coin.Coin;
import com.Andrei.Drink.Drink;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

/**
 * Simulates user-vending machine interactions and its characteristics
 * @author acozma
 *
 */
public abstract interface VendingMachine
{
	
  /**
  * Method that checks if the vending machine is empty.
  * @return true if drink stock is empty
  */
  public abstract boolean isEmpty();

  
  /**
   * Add a new type of drink.
   * @param newDrink - the  new drink
   * @param quantity - number of new drinks
   */
  public abstract void addDrinks(Drink newDrink,int quantity);
  
  /**
   * Add a new type of coin to the coinsStock.
   * @param aCoin - the coin to be added
   * @param quantity - the number of coins to be added
   */
  public abstract void addCoins(Coin newCoin,int quantity);

  /**
   * Method that calculates the change the vending machine has to give back to the user.
   * @param price - the price of the drink
   * @param credit - the available credit 
   * @return the change coin stock 
   */
  public Map<Coin,Integer> calculateChange(BigDecimal price, BigDecimal credit);

  /**
   * Simulate buying a drink.
   * @param drinkName - the type of the drink
   * @param coins - the list of inserted coins
   * @return the drink and change from the vending machine
   */
  public DrinkAndChange buyDrink(String drinkName, List<Coin> coins);
  
}
