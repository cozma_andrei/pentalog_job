package com.Andrei.app;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import com.Andrei.Coin.Coin;
import com.Andrei.Coin.CoinStockImpl;
import com.Andrei.Drink.Drink;
import com.Andrei.Drink.DrinkStockImpl;

/**
 * Class which simulates the features of a Vender Machine. 
 * View component
 * @author acozma
 *
 */
@SuppressWarnings("rawtypes")
public class TestVenderMachine
{
	static DrinkStockImpl drinkStock;
	static CoinStockImpl coinStock;
  
  /**
   * Method that initializes the vending machine with available drinks and coins.
   */
  public static void init()
  {
	  Map<Drink,Integer> drinksStock = new HashMap<>();
	  Map<Coin,Integer> coinsStock = new HashMap<>();
	  
	    //Create coins and add them to the coin stock
	  	Coin c1 = new Coin(AppConstants.COIN_2EURO);
	    Coin c2 = new Coin(AppConstants.COIN_1EURO);
	    Coin c3 = new Coin(AppConstants.COIN_50CENT);
	    Coin c4 = new Coin(AppConstants.COIN_10CENT);
	    
	   
	    coinsStock.put(c1, AppConstants.STANDARD_STACK_OF_COINS);
	    coinsStock.put(c2, AppConstants.STANDARD_STACK_OF_COINS);
	    coinsStock.put(c3, AppConstants.STANDARD_STACK_OF_COINS);
	    coinsStock.put(c4, AppConstants.STANDARD_STACK_OF_COINS);
	    coinStock = new CoinStockImpl(coinsStock);
	    
	    //Create drinks and add them to the drinks stock
	    Drink cola = new Drink(1, AppConstants.COLA, AppConstants.COLA_PRICE);
	    Drink fanta = new Drink(2, AppConstants.FANTA, AppConstants.FANTA_PRICE);
	    Drink sprite = new Drink(3, AppConstants.SPRITE, AppConstants.SPRITE_PRICE);
	    
	    drinksStock.put(cola,4);
	    drinksStock.put(fanta,10);
	    drinksStock.put(sprite,20);
	    drinkStock = new DrinkStockImpl(drinksStock);
  }
  
  /**
   * Method that shows current stocks.
   */
  public static void showStocks()
  {
	  System.out.println("Id\tDrink\tAvailable\tPrice(Euro)");
	  for (Iterator it = drinkStock.getDrinkStock().entrySet().iterator(); it.hasNext(); )
	    {
	      Map.Entry p = (Map.Entry)it.next();
	      Drink d = (Drink)p.getKey();
	      Integer value = (Integer)p.getValue();
		  System.out.format("%d\t", new Object[] { Integer.valueOf(d.getId())});
		  System.out.format("%s\t\t", new Object[] { d.getName() });
		  System.out.format("%d\t\t", new Object[] { value});
		  System.out.format("%.1f", new Object[] { Double.valueOf(d.getPrice()) });
		  System.out.println();
	  }
	  System.out.println("Coin value(Euro)\tAvailable change");
	  for (Iterator it = coinStock.getCoinStock().entrySet().iterator(); it.hasNext(); )
	    {
		  Map.Entry p = (Map.Entry)it.next();
	      Coin c = (Coin)p.getKey();
	      Integer value = (Integer)p.getValue();
	      System.out.format("%.1f\t\t\t", new Object[] { c.getCoinValue() });
	      System.out.format("%d", new Object[] { value});
	      System.out.println();
	    }
	  
	  
  }
  /**
   * Method that simulates the insertion of coins.
   * @return a list of coin stocks
   */
  public static List<Coin> insertCoins() 
  {
		
	  	Scanner console = new Scanner(System.in);
		System.out.println("Insert coins in format : x Coins of x and press Start:");
	    String coins = console.nextLine();
	    List<String> coinList = new ArrayList<String>();
	    List<Coin> insertedCoins = new ArrayList<>();
	    while (!coins.equals("Start"))
	    {
	      if (!coins.equals("Start"))
	        coinList.add(coins);
	      coins = console.nextLine();
	    }

	    
	    for (Iterator<String> it = coinList.iterator(); it.hasNext(); )
	    {
	      String m = (String)it.next();
	      String[] n = new String[10];

	      n = m.split(" ");
	      Coin c = new Coin(new BigDecimal(Double.parseDouble(n[3])));
	      if(!verifyCoin(c)) System.out.println("The coin inserted doesn't exist");
	      else
	      {
	     
	    	  int size = Integer.parseInt(n[0]);
	    	  for(int i = 0 ; i<size;i++)
	    	  insertedCoins.add(c);
	      }
	    }
	    console.close();
	    return insertedCoins;
	}
  private static boolean verifyCoin(Coin verifiedCoin) {
	for(Iterator it = coinStock.getCoinStock().entrySet().iterator();it.hasNext();)
	{
		Map.Entry p = (Map.Entry)it.next();
		Coin existingCoin = (Coin) p.getKey();
		if(existingCoin.getCoinValue().equals(verifiedCoin.getCoinValue()))
				return true;
	}
	return false;
}

/**
   * Method that simulates item selection by the user
   * @param kiosk - the vending machine
   */
  public static void selectItem(VendingMachine kiosk) 
  {
   int option;
   String drinkName="";
   DrinkAndChange  returnedItems=null;
   Scanner console = new Scanner(System.in);
   System.out.println("Select a drink and insert coins in the vending machine : \n");
   for (Iterator it = drinkStock.getDrinkStock().entrySet().iterator(); it.hasNext(); )
   {
     Map.Entry p = (Map.Entry)it.next();
     Drink d = (Drink)p.getKey();
   	 System.out.println(d.getId()+" - " + d.getName()+" "+d.getPrice()+" euro" );
   }
   boolean ok=false;
   do
   {
	   option = console.nextInt();
	   if(option<1 || option>drinkStock.getDrinkStock().size())
	   {
		   System.out.println("Invalid drink! Please try again");
	   }
	   else ok = true;
   }
   while(ok==false); 
   for (Iterator it = drinkStock.getDrinkStock().entrySet().iterator(); it.hasNext(); )
   { 
	   Map.Entry p = (Map.Entry)it.next();
	   Drink d = (Drink)p.getKey();
   	if(option==d.getId())
   		drinkName = d.getName();
   }
   System.out.println("You have chosen : " + option);
   try 
   {
   	List<Coin> insertedCoins = insertCoins();
   	if(insertedCoins == null) System.out.println("Insuficient credit");
   	returnedItems=kiosk.buyDrink(drinkName, insertedCoins);
	} 
   catch (Exception e) 
   {		
		e.printStackTrace();
   }
   List<Coin> change = returnedItems.getReturnedCoins();
   System.out.println("Your change : ");
   for(Coin c : change)
   {
	   System.out.println(c.getCoinValue()+" euro");
   }
   Drink d = returnedItems.getReturnedDrink();
   System.out.println("Your drink : " + d.getName());
   System.out.println("Have a nice day !");
   console.close();
}
  
  public static void main(String[] args)
  {
	 
    
    init();
    VendingMachine kiosk = new VendingMachineImpl(coinStock,drinkStock);
    showStocks();
    selectItem(kiosk);  
    showStocks();
  }   
}