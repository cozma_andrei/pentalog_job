package com.Andrei.app;

import java.math.BigDecimal;

/**
 * Helper class for defining constant values.
 * @author acozma
 *
 */
public class AppConstants{
	public final static String COLA = "Cola";
	public final static String SPRITE = "Sprite";
	public final static String FANTA = "Fanta";
	public final static String PEPSI = "Pepsi";
	public final static double COLA_PRICE = 0.7;
	public final static double FANTA_PRICE = 0.3;
	public final static double SPRITE_PRICE = 0.4;
	public final static double PEPSI_PRICE = 0.7;
	public final static int STANDARD_STACK_OF_COINS = 10;
	public final static int STANDARD_STACK_OF_DRINKS = 10;
	public final static BigDecimal COIN_2EURO = new BigDecimal(2).setScale(1,BigDecimal.ROUND_HALF_DOWN);
	public final static BigDecimal COIN_1EURO = new BigDecimal(1).setScale(1,BigDecimal.ROUND_HALF_DOWN);
	public final static BigDecimal COIN_50CENT = new BigDecimal(0.5).setScale(1,BigDecimal.ROUND_HALF_DOWN);
	public final static BigDecimal COIN_10CENT = new BigDecimal(0.1).setScale(1,BigDecimal.ROUND_HALF_DOWN);


}
