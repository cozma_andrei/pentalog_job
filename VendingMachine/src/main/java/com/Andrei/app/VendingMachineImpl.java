package com.Andrei.app;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;



import com.Andrei.Coin.Coin;
import com.Andrei.Coin.CoinStock;
import com.Andrei.Coin.CoinStockImpl;
import com.Andrei.Drink.Drink;
import com.Andrei.Drink.DrinkStock;
import com.Andrei.Drink.DrinkStockImpl;

/**
 * Controller component
 * Class which simulates user-vending machine interactions
 * @author acozma
 *
 */
@SuppressWarnings("rawtypes")
public class VendingMachineImpl
  implements VendingMachine
{
	/**
	 * The drink stock.
	 */
	private DrinkStock drinksStock = new DrinkStockImpl();
	/**
	 * The coin stock
	 */
	private CoinStock coinsStock = new CoinStockImpl();

	/**
	 * Constructor for the vending machine.
	 * @param coinsStock - the coin stock
	 * @param drinksStock - the drink stock
	 */
  public VendingMachineImpl(CoinStock coinsStock,DrinkStock drinksStock)
  {
	  this.coinsStock = coinsStock;
	  this.drinksStock = drinksStock;
  }
  
  /**
   * @see VendingMachine#isEmpty
   */
  public boolean isEmpty()
  {
    Map<Drink, Integer> drinkStock = drinksStock.getDrinkStock();
    for (Iterator it = drinkStock.entrySet().iterator(); it.hasNext(); )
    {
      Map.Entry p = (Map.Entry)it.next();

      Integer quantity = (Integer)p.getValue();
      if(quantity!=0) return false;
      
    }
    return true;
  }

  /**
   * @see VendingMachine#addDrinks
   */
  public void addDrinks(Drink newDrink,int quantity)
  {
	  boolean ok = false;
	  for (Iterator it = drinksStock.getDrinkStock().entrySet().iterator(); it.hasNext(); )
	    {
	      Map.Entry p = (Map.Entry)it.next();
	      Drink aDrink = (Drink)p.getKey();
	      Integer value = (Integer)p.getValue();
	      if(aDrink.getName().equals(newDrink.getName()))
	      { 
	    	  ok = true;
	    	  drinksStock.getDrinkStock().put(aDrink, value + quantity);
	    	  break;
	      }
	      
	    }
	  if(ok==false)
		  drinksStock.getDrinkStock().put(newDrink, quantity);
  }

  /**
   * @see VendingMachine#addCoins
   */
  public void addCoins(Coin newCoin, int quantity)
  {
	  boolean ok = false;
	  for (Iterator it = coinsStock.getCoinStock().entrySet().iterator(); it.hasNext(); )
	    {
	      Map.Entry p = (Map.Entry)it.next();
	      Coin aCoin = (Coin)p.getKey();
	      Integer value = (Integer)p.getValue();
	      if(aCoin.getCoinValue().equals(newCoin.getCoinValue()))
	      { 
	    	  ok = true;
	    	  coinsStock.getCoinStock().put(aCoin, value + quantity);
	    	  break;
	      }
	      
	    }
	  if(ok==false)
		  coinsStock.getCoinStock().put(newCoin, quantity);
  }

 
  
  /**
   * Method that calculates the user's credit from the inserted coins.
   * @param insertedCoins - the inserted coins
   * @return the credit
   */
  public BigDecimal calculateCredit(List<Coin> insertedCoins)
  {
	  BigDecimal credit = BigDecimal.ZERO;
	  for (Iterator it = insertedCoins.iterator(); it.hasNext(); )
	    {
	      
	      Coin type = (Coin)it.next();
	      credit = credit.add((type.getCoinValue()));
	      
	    }
	  return credit;
  }
  
  
  /**
   * Method that updates the stock of drinks and coins.
   * @param drinkName - the drink that was purchased 
   * @param insertedCoins - the inserted coins
   * @param change - the given change that will be put in the coinsStock
   */
  public void updateStocks(String drinkName,Map<Coin,Integer> insertedCoins,Map<Coin,Integer> change)
  {
	  //Decrease the number of drinks with the drinkName
	  for (Iterator it = drinksStock.getDrinkStock().entrySet().iterator(); it.hasNext(); )
	  {
		  Map.Entry p = (Map.Entry)it.next();
	      Drink d = (Drink)p.getKey();
	      Integer value = (Integer)p.getValue();
	      if(d.getName().equals(drinkName))
	      { 
	    	 drinksStock.getDrinkStock().put(d, value-1);
	      }
	  }
	  for (Iterator it = insertedCoins.entrySet().iterator(); it.hasNext(); )
	  {
		  Map.Entry p = (Map.Entry)it.next();
	      Coin c = (Coin)p.getKey();
	      Integer value = (Integer)p.getValue();
	      Integer oldValue = 0 ;
	      Coin c2=null;
	      for(Iterator it2 = coinsStock.getCoinStock().entrySet().iterator();it2.hasNext();)
	      {
	    	  Map.Entry q = (Map.Entry)it2.next();
	    	   c2 = (Coin) q.getKey();
	    	  if(c2.getCoinValue().equals(c.getCoinValue()))
	    		  {
	    		  	oldValue = coinsStock.getCoinStock().get(c2);
	    		  	break;
	    		  }
	      }
	      
	      if(value>0)
	      coinsStock.getCoinStock().put(c2, value+oldValue);
	  }
	  if(change != null)
	  {
		  for (Iterator it = change.entrySet().iterator(); it.hasNext(); )
		  {
			  Map.Entry p = (Map.Entry)it.next();
		      Coin c = (Coin)p.getKey();
		      Integer value = (Integer)p.getValue();
		      Integer oldValue = 0 ;
		      Coin c2 = null;
		      for(Iterator it2 = coinsStock.getCoinStock().entrySet().iterator();it2.hasNext();)
		      {
		    	  Map.Entry q = (Map.Entry)it2.next();
		    	   c2 = (Coin) q.getKey();
		    	  if(c2.getCoinValue()==c.getCoinValue())
		    		  {
		    		  	oldValue = coinsStock.getCoinStock().get(c2);
		    		  	break;
		    		  }
		      }
		      if(value>0)
		    	  coinsStock.getCoinStock().put(c2, oldValue-value);
		  }
	  }
		  		
	  
  }
  
 
  /**
   * Method that gives the change, if possible, back to the user. 
   * @param drinkName - the name of the drink
   * @param insertedCoins - list of inserted coins
   * @param change - the change grouped as stock coins
   * @param price - the price of the drink
   * @param credit - the available credit
   * @return message
   */
   private String giveChange(String drinkName,
		Map<Coin,Integer> insertedCoins, BigDecimal price, BigDecimal credit) 
   {
	   Map<Coin,Integer> change=null;
	   String message="";
	if(credit.compareTo(price)==0)
	{
		//No change to give
		message = "OK";
		updateStocks(drinkName,insertedCoins,change);
		return message;
	}
	else	
		if(credit.compareTo(price)<0)
		{
			message = "Credit is too small ! Please insert more coins";
			return message;
		}
		else
		{
			//Verify change availability
			change = calculateChange(price, credit);
			if(change==null)
			{
				message ="We don't have change to pay";
			}
			else
			{
				message+="Your change :" + credit.subtract(price).toString()+" euro\n";
				 for(Iterator it = change.entrySet().iterator(); it.hasNext(); )
				 {
					 Map.Entry p = (Map.Entry)it.next();
				      Coin c = (Coin)p.getKey();
				      Integer value = (Integer)p.getValue();
					message += c.getCoinValue().toString()+" * "+value+"\n";
				 }
				updateStocks(drinkName,insertedCoins,change);
			}
			
		}
	return message;
}
  
   /**
    * @see VendingMachine#calculateChange
    */
  public Map<Coin,Integer> calculateChange(BigDecimal price, BigDecimal credit)
  {
    BigDecimal change = credit.subtract(price);
    BigDecimal coinValue;
    Map<Coin,Integer> stockChange = null;
   
    //Work with a copy of the coin stock
    Map<Coin,Integer> coinsStockCopy = new HashMap<>();
    
    for(Iterator it = coinsStock.getCoinStock().entrySet().iterator(); it.hasNext(); )
	{
    	Map.Entry p = (Map.Entry)it.next();
	      Coin c = (Coin)p.getKey();
	      Integer value = (Integer)p.getValue();
	      coinsStockCopy.put(c, value);
	}
    
    
    BigDecimal maximumCoinValue;
    // The algorithm is simple : until change is not empty we find the maximum available coin
    // in the coin stack, subtract it from its stack and place it in the change stack.
    // If we ran out of coins and the change wasn't fully given, the max coin will be of course 0, so the vending
    // machine cannot give change, the coin stock won't be affected and the credit will be 'returned' to the user
    while (change.compareTo(BigDecimal.ZERO) != 0)
    {
      maximumCoinValue = BigDecimal.ZERO;
      Coin maxCoin = new Coin(BigDecimal.ZERO);
      for(Iterator it = coinsStockCopy.entrySet().iterator(); it.hasNext(); )
      {
    	  Map.Entry p = (Map.Entry)it.next();
	      Coin c = (Coin)p.getKey();
	      Integer value = (Integer)p.getValue();
	      
    	  coinValue = c.getCoinValue();
    	  if(maximumCoinValue.compareTo(coinValue)<0 && coinValue.compareTo(change)<=0 && value>0)
    	  {
    		  maxCoin = c;
    		  maximumCoinValue = c.getCoinValue();
    	  }
      }
      if (maximumCoinValue.compareTo(BigDecimal.ZERO) == 0)
      {
    	  //We don't have enough coins in stock to give change - we will return the initial credit back to the user
    	  return null;	
      }
      else
      {
    	  //Put the maxCoin in the change stock
    	  if(stockChange == null)
    	  {
    		  stockChange = new HashMap<>();
    		  stockChange.put(maxCoin,1);
    		  coinsStockCopy.put(maxCoin, coinsStockCopy.get(maxCoin)-1);
    	  }
    	  else
    	  {
    		  if(stockChange.get(maxCoin)==null) stockChange.put(maxCoin, 0);
    		  stockChange.put(maxCoin,stockChange.get(maxCoin)+1);
    		  coinsStockCopy.put(maxCoin, coinsStockCopy.get(maxCoin)-1);
    	  }

    			  
    		  
      }  	
	      change = change.subtract(maximumCoinValue);
      }
    //All is good - we can update the coinsStock
    return stockChange;
  }
  
  /**
   * @see VendingMachine#buyDrink
   */
  public DrinkAndChange buyDrink(String drinkName, List<Coin> coins)
  {
		BigDecimal creditAmount = calculateCredit(coins);
		boolean isDrinkAvailable = drinksStock.isAvailable(drinkName);
		if (!isDrinkAvailable) 
			System.out.println("Drink unavailable");
		else
		{
			BigDecimal price = new BigDecimal(drinksStock.getDrink(drinkName).getPrice()).setScale(1,BigDecimal.ROUND_HALF_DOWN);
			BigDecimal requiredChange = creditAmount.subtract(price);
			if (requiredChange.compareTo(BigDecimal.ZERO) < 0) 
				System.out.println("Not enough credit");
			else
			{
				boolean hasChange = coinsStock.hasChange(requiredChange);
				if (!hasChange) 
				{
					System.out.println("change unavailable");
				}
				else
					return deliverDrinkAndChange(drinkName,coins,requiredChange);
			} 
		}
		return null; 
		
		
  }

  public DrinkAndChange deliverDrinkAndChange(String drinkName, List<Coin> credit, BigDecimal requiredChange)
  {
		DrinkAndChange drinkAndChange = new DrinkAndChange();
		Drink drink = drinksStock.extractDrink(drinkName);
		coinsStock.applyCredit(credit);
		List<Coin> change = coinsStock.extractChange(requiredChange);
		drinkAndChange.setReturnedCoins(change);
		drinkAndChange.setReturnedDrink(drink);
		Boolean isValid = validate(drinkAndChange);
		if(!isValid) 
		{
			drinksStock.revert(drinkName);
			coinsStock.revert(credit,change);
			// alert user
			return new DrinkAndChange(credit);
		} 
		else 
			return drinkAndChange;
  }
  
  public boolean validate(DrinkAndChange drinkAndChange) 
  {
	  if(drinkAndChange == null) return false;
	  if(drinkAndChange.getReturnedDrink()!=null && drinkAndChange.getReturnedCoins() == null) return true;
	  if(drinkAndChange.getReturnedDrink()==null) return false;
	  return true;
  }

	

  
  
  
  
}