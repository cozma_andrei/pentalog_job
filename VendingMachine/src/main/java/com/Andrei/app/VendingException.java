package com.Andrei.app;

/**
 * Class that handles exceptions that occur during transactions between user and vending machine.
 * @author acozma
 *
 */
@SuppressWarnings("serial")
public class VendingException extends Exception {
	public VendingException(){}
	public VendingException(String msg)
	{
		super(msg);
	}

}
