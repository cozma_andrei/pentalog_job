package com.Andrei.Coin;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * Class that manages the coin stock
 * @param coinStock -  maps the coin type with its quantity and stores the entire coin stock
 * @author acozma
 *
 */
@SuppressWarnings("rawtypes")
public class CoinStockImpl implements CoinStock {
	 public  Map<Coin,Integer> coinStock;
	 
	public Map<Coin, Integer> getCoinStock() {
		return coinStock;
	}

	public void setCoinStock(Map<Coin, Integer> coinStock) {
		this.coinStock = coinStock;
	}

	public CoinStockImpl()
	{
		this.coinStock = new HashMap<Coin, Integer>();
	}
	
	public CoinStockImpl(Map<Coin,Integer> coinStock)
	{
		this.coinStock = coinStock;
	}
	
	
	@Override
	public void addCoinsToStock(Map<Coin, Integer> coinsToAdd) 
	{
		for (Iterator it = coinsToAdd.entrySet().iterator(); it.hasNext(); )
	    {
	      Map.Entry p = (Map.Entry)it.next();

	      Integer oldQuantity = (Integer)this.coinStock.get(p.getKey());
	      Integer quantity = (Integer)p.getValue();
	      this.coinStock.put((Coin)p.getKey(), Integer.valueOf(oldQuantity.intValue() + quantity.intValue()));
	    }
		
	}
	@Override
	public void removeCoinsFromStock(Map<Coin, Integer> coinsToAdd) 
	{
		for (Iterator it = coinsToAdd.entrySet().iterator(); it.hasNext(); )
	    {
	      Map.Entry p = (Map.Entry)it.next();

	      Integer oldQuantity = (Integer)this.coinStock.get(p.getKey());
	      Integer quantity = (Integer)p.getValue();
	      this.coinStock.put((Coin)p.getKey(), Integer.valueOf(oldQuantity.intValue() - quantity.intValue()));
	    }
		
	}

	/**
	 * @see CoinStock#hasChange
	 */
	@Override
	public boolean hasChange(BigDecimal requiredChange) 
	{

	    BigDecimal coinValue;
	    Map<Coin,Integer> stockChange = null;
	   
	    //Work with a copy of the coin stock
	    Map<Coin,Integer> coinsStockCopy = new HashMap<>();
	    
	    for(Iterator it = this.coinStock.entrySet().iterator(); it.hasNext(); )
		{
	    	Map.Entry p = (Map.Entry)it.next();
		      Coin c = (Coin)p.getKey();
		      Integer value = (Integer)p.getValue();
		      coinsStockCopy.put(c, value);
		}
	    
	    
	    BigDecimal maximumCoinValue;
	    // The algorithm is simple : until change is not empty we find the maximum available coin
	    // in the coin stack, subtract it from its stack and place it in the change stack.
	    // If we ran out of coins and the change wasn't fully given, the max coin will be of course 0, so the vending
	    // machine cannot give change, the coin stock won't be affected and the credit will be 'returned' to the user
	    while (requiredChange.compareTo(BigDecimal.ZERO) != 0)
	    {
	      maximumCoinValue = BigDecimal.ZERO;
	      Coin maxCoin = new Coin(BigDecimal.ZERO);
	      for(Iterator it = coinsStockCopy.entrySet().iterator(); it.hasNext(); )
	      {
	    	  Map.Entry p = (Map.Entry)it.next();
		      Coin c = (Coin)p.getKey();
		      Integer value = (Integer)p.getValue();
		      
	    	  coinValue = c.getCoinValue();
	    	  if(maximumCoinValue.compareTo(coinValue)<0 && coinValue.compareTo(requiredChange)<=0 && value>0)
	    	  {
	    		  maxCoin = c;
	    		  maximumCoinValue = c.getCoinValue();
	    	  }
	      }
	      if (maximumCoinValue.compareTo(BigDecimal.ZERO) == 0)
	      {
	    	  //We don't have enough coins in stock to give change - we will return the initial credit back to the user
	    	  return false;	
	      }
	      else
	      {
	    	  //Put the maxCoin in the change stock
	    	  if(stockChange == null)
	    	  {
	    		  stockChange = new HashMap<>();
	    		  stockChange.put(maxCoin,1);
	    		  coinsStockCopy.put(maxCoin, coinsStockCopy.get(maxCoin)-1);
	    	  }
	    	  else
	    	  {
	    		  if(stockChange.get(maxCoin)==null) stockChange.put(maxCoin, 0);
	    		  stockChange.put(maxCoin,stockChange.get(maxCoin)+1);
	    		  coinsStockCopy.put(maxCoin, coinsStockCopy.get(maxCoin)-1);
	    	  }

	    			  
	    		  
	      }  	
		      requiredChange = requiredChange.subtract(maximumCoinValue);
	      }
	return true;
	}

	@Override
	public void applyCredit(List<Coin> credit) 
	{
		for(Iterator it = credit.iterator();it.hasNext();)
		{
			Coin c = (Coin) it.next();
			for(Iterator it2 = coinStock.entrySet().iterator();it2.hasNext();)
			{
				Map.Entry p = (Map.Entry) it2.next();
				Coin c2 = (Coin) p.getKey();
				Integer value = (Integer) p.getValue();
				if(c.getCoinValue().equals(c2.getCoinValue()))
					this.coinStock.put(c2, value+1);
			}
		}
	}

	@Override
	public List<Coin> extractChange(BigDecimal requiredChange) {
		
	    List<Coin> stockChange = null;
	    BigDecimal maximumCoinValue;
	    while (requiredChange.compareTo(BigDecimal.ZERO) != 0)
	    {
	      maximumCoinValue = BigDecimal.ZERO;
	      Coin maxCoin = new Coin(BigDecimal.ZERO);
	      for(Iterator it = coinStock.entrySet().iterator(); it.hasNext(); )
	      {
	    	  Map.Entry p = (Map.Entry)it.next();
		      Coin c = (Coin)p.getKey();
		      Integer value = (Integer)p.getValue();
		      
	    	  BigDecimal coinValue = c.getCoinValue();
	    	  if(maximumCoinValue.compareTo(coinValue)<0 && coinValue.compareTo(requiredChange)<=0 && value>0)
	    	  {
	    		  maxCoin = c;
	    		  maximumCoinValue = c.getCoinValue();
	    	  }
	      }
	      if (maximumCoinValue.compareTo(BigDecimal.ZERO) == 0)
	      {
	    	  //We don't have enough coins in stock to give change - we will return the initial credit back to the user
	    	  return null;	
	      }
	      else
	      {
	    	  //Put the maxCoin in the change stock
	    	  if(stockChange == null)
	    	  {
	    		  stockChange = new ArrayList<>();
	    		  stockChange.add(maxCoin);
	    		  coinStock.put(maxCoin, coinStock.get(maxCoin)-1);
	    	  }
	    	  else
	    	  {
	    		  if(stockChange.contains(maxCoin)==false) stockChange.add(maxCoin);
	    		  stockChange.add(maxCoin);
	    		  coinStock.put(maxCoin, coinStock.get(maxCoin)-1);
	    	  }

	    			  
	    		  
	      }  	
		      requiredChange = requiredChange.subtract(maximumCoinValue);
	    }
		return stockChange;
	}
	
	 public void revert(List<Coin> credit,List<Coin> change)
	 {
		  for(Coin coin : credit) 
		  {
			  for(Iterator it = coinStock.entrySet().iterator();it.hasNext();)
			  {
				  Map.Entry p = (Map.Entry) it.next();
				  Coin c = (Coin) p.getKey();
				  Integer value  = (Integer) p.getValue();
				  if(c.getCoinValue().equals(coin.getCoinValue()))
					  coinStock.put(c,value-1);
			  }
		  }
		  
		  for(Coin coin : change) 
		  {
			  for(Iterator it = coinStock.entrySet().iterator();it.hasNext();)
			  {
				  Map.Entry p = (Map.Entry) it.next();
				  Coin c = (Coin) p.getKey();
				  Integer value  = (Integer) p.getValue();
				  if(c.getCoinValue().equals(coin.getCoinValue()))
					  coinStock.put(c,value+1);
			  }
		  }
	}
}