package com.Andrei.Coin;

import java.math.BigDecimal;

/**
 * Class that stores the coin type and value.
 * @author acozma
 *
 */
public class Coin
{
  private BigDecimal coinValue;

  /**
   * Constructor for the coin object.
   * @param value - value of the coin
   */
  public Coin(BigDecimal value)
  {
    this.coinValue = value;
    this.coinValue = this.coinValue.setScale(1, 4);
  }

  /**
   * Get the coin value.
   * @return the coin value
   */
  public BigDecimal getCoinValue()
  {
    return this.coinValue;
  }
  
}