package com.Andrei.Coin;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

/**
 * Interface that manages the stock of coins.
 * @author acozma
 *
 */
public interface CoinStock {

	public Map<Coin, Integer> getCoinStock();
	
	public void setCoinStock(Map<Coin,Integer> coinStock);
	/**
	 * Add a number of Coins to the Coin stock.
	 * @param coinsToAdd - a map containing a number of coins to add to the stock
	 */
	public void addCoinsToStock(Map<Coin, Integer> coinsToAdd);
	/**
	 * Remove a number of Coins from the Coin stock.
	 * @param coinsToAdd - a map containing a number of coins to remove from the stock
	 */
	public void removeCoinsFromStock(Map<Coin, Integer> coinsToAdd);

	/**
	 * Method that verifies if the vending machine can give the user the change
	 * @param coins - list of inserted coins
	 * @return true if the machine can give change
	 */
	public boolean hasChange(BigDecimal requiredChange);

	/**
	 * Update the coin stock by adding the newly inserted coins
	 * @param credit - a list of inserted coins
	 */
	public void applyCredit(List<Coin> credit);

	/**
	 * Method that extracts the change from the vending machine
	 * @param requiredChange the amount of change to give to the user
	 * @return the list of coins to the user
	 */
	public List<Coin> extractChange(BigDecimal requiredChange);

	public void revert(List<Coin> credit,List<Coin> change);
}
