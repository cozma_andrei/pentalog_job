package com.Andrei.app;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.Andrei.Coin.Coin;
import com.Andrei.Coin.CoinStock;
import com.Andrei.Coin.CoinStockImpl;
import com.Andrei.Drink.Drink;
import com.Andrei.Drink.DrinkStock;
import com.Andrei.Drink.DrinkStockImpl;
import com.Andrei.app.AppConstants;
import com.Andrei.app.VendingException;
import com.Andrei.app.VendingMachineImpl;
import junit.framework.TestCase;

/**
 * Class that tests the methods created in the VendingMachineImpl using jUnit asserts.
 * @author acozma
 *
 */
@SuppressWarnings("rawtypes")
public class TestVenderMachineImpl extends TestCase{
	
	DrinkStock drinksStocks = new DrinkStockImpl();
	CoinStock coinsStocks = new CoinStockImpl();
	
	public void setUp() throws Exception {
		Drink fanta = new Drink(2, AppConstants.FANTA, AppConstants.FANTA_PRICE);
		Drink cola =  new Drink(1, AppConstants.COLA, AppConstants.COLA_PRICE);
		Drink sprite = new Drink(3, AppConstants.SPRITE, AppConstants.SPRITE_PRICE);
	
		drinksStocks.getDrinkStock().put(cola,4);
		drinksStocks.getDrinkStock().put(fanta,1);
		drinksStocks.getDrinkStock().put(sprite,1);
	
		Coin c1 = new Coin(AppConstants.COIN_2EURO);
		Coin c2 = new Coin(AppConstants.COIN_1EURO);
		Coin c3 = new Coin(AppConstants.COIN_50CENT);
		Coin c4 = new Coin(AppConstants.COIN_10CENT);
		
		
		coinsStocks.getCoinStock().put(c1, AppConstants.STANDARD_STACK_OF_COINS);
		coinsStocks.getCoinStock().put(c2, AppConstants.STANDARD_STACK_OF_COINS);
		coinsStocks.getCoinStock().put(c3, AppConstants.STANDARD_STACK_OF_COINS);
		coinsStocks.getCoinStock().put(c4, AppConstants.STANDARD_STACK_OF_COINS);
	}
	/**
	 * Verify if a drink is present in the drink stock.
	 */
	public void testDrinkAvailable() {
		//verify if drink is available
		VendingMachine vmi = new VendingMachineImpl(coinsStocks, drinksStocks);
		assertNotNull("Vending machine was not created",vmi);
		String aDrink = AppConstants.COLA;
		//The drink is available. An index different from -1 will be returned
		assertEquals("The drink "+aDrink+" doesn't exist in the stock list and it should\n",true, drinksStocks.isAvailable(aDrink));
	}
	/**
	 *  Verify that a drink is not available in the drink stock.
	 */
	public void testDrinkUnavailable() {
		//verify if drink is available
		String aDrink = AppConstants.PEPSI;
		VendingMachine vmi = new VendingMachineImpl(coinsStocks, drinksStocks);
		//test if vmi is created
		assertNotNull("Vending machine was not created",vmi);
		//The  drink is unavailable. The returned index is -1
		assertEquals("The drink "+aDrink+" exists in the stock list and it shouldn't\n",false,drinksStocks.isAvailable(aDrink));
	}
	
	/**
	 * Testing the buyDrink method. 
	 */
	public void testBuyDrink()
	{
		VendingMachine vmi = new VendingMachineImpl(coinsStocks,drinksStocks);
		String drinkName = AppConstants.COLA;
		List<Coin> insertedCoins = new ArrayList<>();
		//Insert 2 euro 
		insertedCoins.add(new Coin(AppConstants.COIN_1EURO));
		insertedCoins.add(new Coin(AppConstants.COIN_1EURO));
		DrinkAndChange d = null;
		try 
		{
			d = vmi.buyDrink(drinkName, insertedCoins) ;
		} 
		catch (Exception e) {
			e.printStackTrace();
		}
		assertNotNull(d);
	}
	
	
	/**
	 * Verify that the credit is enough to pay the drink.
	 */
	public void testSuficientCredit() {
		VendingMachine vmi = new VendingMachineImpl(coinsStocks, drinksStocks);
		DrinkAndChange d = null;
		List<Coin> insertedCoins = new ArrayList<>();
		//Insert 2 euro 
		insertedCoins.add(new Coin(AppConstants.COIN_1EURO));
		insertedCoins.add(new Coin(AppConstants.COIN_1EURO));
		try 
		{
			d = vmi.buyDrink(AppConstants.COLA, insertedCoins);
		} 
		catch (Exception e) {
			// 
			e.printStackTrace();
		}
		assertNotNull(d);
	}

	/**
	 * Verify that the credit is not enough to pay the drink.
	 */
	public void testInsuficientCredit() {
		VendingMachine vmi = new VendingMachineImpl(coinsStocks, drinksStocks);
		List<Coin> insertedCoins = new ArrayList<>();
		//Insert 20 euro cents
		DrinkAndChange d = null;
		insertedCoins.add(new Coin(AppConstants.COIN_10CENT));
		insertedCoins.add(new Coin(AppConstants.COIN_10CENT));
		try 
		{
			d = vmi.buyDrink(AppConstants.COLA, insertedCoins);
		} 
		catch (Exception e) {
			 
			e.printStackTrace();
		}
		assertNull(d);
	}
	
	
	/**
	 * Verify that the Vending machine is empty.
	 */
	public void testStockIsEmpty()
	{
		VendingMachine vmi = new VendingMachineImpl(coinsStocks, drinksStocks);
		vmi.addCoins(new Coin(AppConstants.COIN_10CENT),10);
		List<Coin> insertedCoins = new ArrayList<>();
		//Insert 2 euro 
		insertedCoins.add(new Coin(AppConstants.COIN_1EURO));
		insertedCoins.add(new Coin(AppConstants.COIN_1EURO));
		try {
				vmi.buyDrink(AppConstants.COLA, insertedCoins);
				vmi.buyDrink(AppConstants.COLA, insertedCoins);
				vmi.buyDrink(AppConstants.COLA, insertedCoins);
				vmi.buyDrink(AppConstants.COLA, insertedCoins);
				vmi.buyDrink(AppConstants.FANTA, insertedCoins);
				vmi.buyDrink(AppConstants.SPRITE, insertedCoins);
		} catch (Exception e) {
			
			e.printStackTrace();
		}
		
		assertTrue("Vending machine is not empty",vmi.isEmpty());
	}
	/**
	 * Verify that the vending machine doesn't have change to give to the user.
	 * @throws Exception
	 */
	public void testNotAvailableChange() throws Exception {
		DrinkAndChange d = null;
		try 
		{
			VendingMachine vmi = new VendingMachineImpl(coinsStocks,drinksStocks);
			List<Coin> insertedCoins = new ArrayList<>();
			
			//Insert 2 euro 
			insertedCoins.add(new Coin(AppConstants.COIN_10CENT));
			insertedCoins.add(new Coin(AppConstants.COIN_10CENT));	
			vmi.buyDrink(AppConstants.COLA, insertedCoins);
			vmi.buyDrink(AppConstants.COLA, insertedCoins);
			vmi.buyDrink(AppConstants.COLA, insertedCoins);
		    d = vmi.buyDrink(AppConstants.COLA, insertedCoins);
		}
		catch (Exception e) 
		{
			
			e.printStackTrace();
		}
		assertNull("We don't have change to pay", d);
	}
	
	
	
	

}
